﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ControllersAndActionMethods.Controllers
{
    public class BasicController : IController
    {
        public void Execute(RequestContext requestContext)
        {
            // Output from routing that matched this controller
            string controller = (string)requestContext.RouteData.Values["controller"];
            string action = (string)requestContext.RouteData.Values["action"];
            string id = (string)requestContext.RouteData.Values["id"];

            // This method must decide how to handle the request, normally by figuring out which 
            // action method to invoke, using model binders to instantiate values to pass to it,
            // and then invoking it.  Here we'll just do something super simple

            // Configure the HTTP response. 
            if (action.Equals("json"))
            {
                requestContext.HttpContext.Response.ContentType = "application/json";
                requestContext.HttpContext.Response.ContentEncoding = System.Text.Encoding.UTF8;
                requestContext.HttpContext.Response.Headers["MySpecialHeader"] = "Kilroy was here!";
                requestContext.HttpContext.Response.Write(
                string.Format("{{\"Controller\": \"{0}\",\"Action\": \"{1}\",\"id\": \"{2}\"}}", controller, action, id)
                );
            }
            else
            {
                requestContext.HttpContext.Response.Write(
                string.Format("Controller: {0}\nAction: {1}\nid: {2}", controller, action, id)
                );
            } 
        }
    }
}